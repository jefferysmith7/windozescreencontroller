﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindozeScreenController
{
    public partial class WindozeScreenController : Form
    {
        public Controller myController;
        
        public WindozeScreenController()
        {
            InitializeComponent();
            this.myController = new Controller(this);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            this.myController.SaveCurrentConfiguration(saveNameComboBox.Text);
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            this.myController.FetchConfiguration(saveNameComboBox.Text);
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            this.myController.DeleteConfiguration(saveNameComboBox.Text);
        }

        private void saveNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
