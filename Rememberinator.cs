﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using HWND = System.IntPtr;

namespace WindozeScreenController
{
    public class Rememberinator
    {
        private const string SETTINGS_FILE = "WinScreenController.settings";
        private WindozeScreenController myWSC;
        private readonly HWND shellWindow = GetShellWindow();
        public IDictionary<string, Configurator> SettingsToRemember;
        public Configurator currentConfiguration;

        public Rememberinator(WindozeScreenController wsc)
        {
            myWSC = wsc;
            currentConfiguration = GetCurrentConfiguration();
            SettingsToRemember = new Dictionary<string, Configurator>();
            Load();
            //UpdateCurrentScreenDisplay();
            //UpdateWindowCountLabel();
        }

        public Configurator GetCurrentConfiguration(string configName = "Current Configuration")
        {
            Configurator currentConfig = new Configurator();
            currentConfig.name = configName;
            currentConfig.windowDetails = GetCurrentWindows();
            currentConfig.screenDetails = GetCurrentScreens();
            return currentConfig;
        }

        public void AddConfiguration(string configName)
        {
            Configurator configToAdd = GetCurrentConfiguration(configName);

            if (SettingsToRemember.ContainsKey(configName))
            {
                SettingsToRemember[configName] = configToAdd;
            }
            else
            {
                SettingsToRemember.Add(configName, configToAdd);
            }
        }

        public void RestoreConfiguration(string configName)
        {
            currentConfiguration = GetCurrentConfiguration();
            Configurator savedConfiguration = SettingsToRemember[configName];
            savedConfiguration.applyConfiguration(currentConfiguration);
        }

        public void RemoveConfiguration(string configName)
        {
            if (SettingsToRemember.ContainsKey(configName))
            {
                SettingsToRemember.Remove(configName);
                Save();
            }
            else
            {
                Console.WriteLine("WARNING: No configuration with that name found: {0}", configName);
            }
        }

        private void UpdateSaveNameComboBox()
        {
            if (SettingsToRemember.Count > 0)
            {
                myWSC.saveNameComboBox.DataSource = new BindingSource(SettingsToRemember, null);
                myWSC.saveNameComboBox.DisplayMember = "Key";
                myWSC.saveNameComboBox.ValueMember = "Key";
            }
            else
            {
                Console.WriteLine("WARNING: Settings file was empty");
            }
        }

        private void UpdateCurrentScreenDisplay()
        {
            //myWSC.numberOfScreensTextBox.Text = currentConfiguration.screenDetails.Count.ToString();

            //foreach (ScreenDetail screen in currentConfiguration.screenDetails)
            //{
            //    ListViewItem lvItem = new ListViewItem(screen.DeviceName);
            //    lvItem.SubItems.Add(screen.Primary.ToString());
            //    myWSC.screensListView.Items.Add(lvItem);
            //}
        }

        private void UpdateWindowCountLabel()
        {
            //myWSC.numberOfWindowsTextBox.Text = currentConfiguration.windowDetails.Count.ToString();
        }

        public void Load()
        {
            if (File.Exists(SETTINGS_FILE))
            {
                try 
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    FileStream readerFS = new FileStream(SETTINGS_FILE, FileMode.Open, FileAccess.Read);
                    SettingsToRemember = (Dictionary<string, Configurator>) formatter.Deserialize(readerFS);
                    readerFS.Close();
                    Console.WriteLine("INFO: Settings have been loaded.");
                    UpdateSaveNameComboBox();
                }
                catch (Exception)
                {
                    Console.WriteLine("ERROR: Unable to load {0} for reading.", SETTINGS_FILE);
                }
            }
            else
            {
                Console.WriteLine("WARNING: No settings file exists yet.");
            }
        }

        public void Save()
        {
            if (myWSC.saveNameComboBox.Text.Length > 0)
            {
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    FileStream writerFS = new FileStream(SETTINGS_FILE, FileMode.OpenOrCreate, FileAccess.Write);
                    formatter.Serialize(writerFS, SettingsToRemember);
                    writerFS.Close();
                    Console.WriteLine("INFO: Settings have been saved.");
                    UpdateSaveNameComboBox();
                }
                catch
                {
                    Console.WriteLine("ERROR: Unable to save {0}", SETTINGS_FILE);
                }
            }
            else
            {
                Console.WriteLine("WARNING: Unable to save without a name.");
            }
        }

        public List<WindowDetail> GetCurrentWindows()
        {
            List<WindowDetail> CurrentWindows = new List<WindowDetail>();

            EnumWindows(delegate (HWND hWnd, int lParam)
            {
                if (hWnd == shellWindow) return true;
                if (!IsWindowVisible(hWnd)) return true;

                int length = GetWindowTextLength(hWnd);
                if (length == 0) return true;

                StringBuilder builder = new StringBuilder(length);
                GetWindowText(hWnd, builder, length + 1);

                CurrentWindows.Add(new WindowDetail(hWnd, builder.ToString()));
                return true;

            }, 0);

            return CurrentWindows;
        }

        public List<ScreenDetail> GetCurrentScreens()
        {
            List<ScreenDetail> CurrentScreens = new List<ScreenDetail>();

            foreach (Screen screen in Screen.AllScreens)
            {
                CurrentScreens.Add(new ScreenDetail(screen));
            }

            return CurrentScreens;
        }

        private delegate bool EnumWindowsProc(HWND hWnd, int lParam);

        [DllImport("USER32.DLL")]
        private static extern bool EnumWindows(EnumWindowsProc enumFunc, int lParam);

        [DllImport("USER32.DLL")]
        private static extern int GetWindowText(HWND hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("USER32.DLL")]
        private static extern int GetWindowTextLength(HWND hWnd);

        [DllImport("USER32.DLL")]
        private static extern bool IsWindowVisible(HWND hWnd);

        [DllImport("USER32.DLL")]
        private static extern IntPtr GetShellWindow();

    }
}
