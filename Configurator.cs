﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WindozeScreenController
{
    [Serializable]
    public class Configurator
    {
		public string name;
		public List<ScreenDetail> screenDetails = new List<ScreenDetail>();
		public List<WindowDetail> windowDetails = new List<WindowDetail>();

		public Configurator() 
		{
			
		}

		public void applyConfiguration(Configurator currentConfiguration)
		{
			if (CompareScreens(currentConfiguration))
			{
				foreach (WindowDetail window in windowDetails)
				{
					WindowDetail currentWindow = GetWindowByName(window.winShortName, currentConfiguration.windowDetails);

					if (currentWindow != null && !currentWindow.IsEqualWinCoords(window.winCoords))
					{
						currentWindow.winCoords = window.winCoords;
						currentWindow.SetWinCoords();
					}
				}
			}
			
		}

		private static WindowDetail GetWindowByName(string nameToFind, List<WindowDetail> windows)
		{
			foreach (WindowDetail savedWindow in windows)
			{
				if (savedWindow.winShortName == nameToFind)
				{
					return savedWindow;
				}

			}

			return null;
		}

		private static WindowDetail GetWindowByExe(string exeToFind, List<WindowDetail> windows)
		{
			foreach (WindowDetail savedWindow in windows)
			{
				if (savedWindow.winExecutable == exeToFind)
				{
					return savedWindow;
				}
			}

			return null;
		}

		private bool CompareScreens(Configurator currentConfiguration)
		{
			if (screenDetails.Count != currentConfiguration.screenDetails.Count)
			{
				Console.WriteLine("ERROR: Number of screens does not match.  Current: {0}  Saved: {1}",
					currentConfiguration.screenDetails.Count, screenDetails.Count);
				return false;
			}
			for (int s = 0; s <= screenDetails.Count - 1; s++)
			{
				if (screenDetails[s].DeviceName != currentConfiguration.screenDetails[s].DeviceName)
				{
					Console.WriteLine("ERROR: Screen device name does not match.  Current: {0}  Saved: {1}",
						currentConfiguration.screenDetails[s].DeviceName, screenDetails[s].DeviceName);
					return false;
				}
			}

			return true;
		}

	} // End public class Configurator
}
