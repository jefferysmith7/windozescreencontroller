﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using HWND = System.IntPtr;

namespace WindozeScreenController
{
	[Serializable]
	public class WindowDetail
	{
		public HWND hWnd;
		public string winName;
		public string winShortName;
		public Rectangle winCoords;
		public string winExecutable;
		const uint SWP_NOZORDER = 0x0004;

		public WindowDetail(HWND windowHandle, string windowName)
		{
			hWnd = windowHandle;
			winName = windowName;
			winShortName = GetShortName(windowName);
			winCoords = GetWinCoords();
			winExecutable = GetProcessPath(hWnd);
		}

		public Rectangle GetWinCoords()
		{
			Rect lpRect;
			if (!GetWindowRect(hWnd, out lpRect)) return new Rectangle();

			Rectangle windowRectangle = new Rectangle
			{
				X = lpRect.Left,
				Y = lpRect.Top,
				Width = lpRect.Right - lpRect.Left + 1,
				Height = lpRect.Bottom - lpRect.Top + 1
			};

			return windowRectangle;
		}

		public void SetWinCoords()
		{
			if (hWnd != IntPtr.Zero)
			{
				SetWindowPos(hWnd, IntPtr.Zero, winCoords.X, winCoords.Y, winCoords.Width, winCoords.Height, SWP_NOZORDER);
			}
		}

		public static string GetProcessPath(IntPtr hwnd)
		{
			try
			{
				GetWindowThreadProcessId(hwnd, out uint pid);
				Process proc = Process.GetProcessById((int)pid);
				return proc.MainModule.FileName.ToString();
			}
			catch (Exception ex)
			{
				Console.WriteLine("ERROR: Unable to retrive pid for window handle {0}: {1}", hwnd.ToString(), ex.Message.ToString());
				return ex.Message.ToString();
			}
		}

		public static string GetShortName(string longName)
		{
			string[] nameParts = longName.Split('-');
			string shortName = nameParts[nameParts.Length - 1].Trim();
			return shortName;
		}

		public bool IsEqualWinCoords(Rectangle coordsToCompare)
		{
			if (winCoords.X != coordsToCompare.X) { return false; }
			if (winCoords.Y != coordsToCompare.Y) { return false; }
			if (winCoords.Width != coordsToCompare.Width) { return false; }
			if (winCoords.Height != coordsToCompare.Height) { return false; }
			return true;
		}

		public struct Rect
		{
			public int Left { get; set; }
			public int Top { get; set; }
			public int Right { get; set; }
			public int Bottom { get; set; }
		}

		[DllImport("user32.dll", SetLastError = true)]
		public static extern bool GetWindowRect(IntPtr hwnd, out Rect lpRect);

		[DllImport("USER32.DLL", SetLastError = true)]
		static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

		[DllImport("USER32.DLL", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern int GetWindowThreadProcessId(IntPtr handle, out uint processId);

	} // End public class WindowDetail

}
