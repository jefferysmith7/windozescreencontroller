# Windoze Screen Controller

A small utility for remembering where your windows belong in multi-screen environments.  When using a notebook PC, I have found that my system often "forgets" where my application windows were when using multi-screen set ups.  This application simply records the postion and sizes of the windows currently open and stores it so that the those settings can be restored later.



WindozeScreenController --> Controller --> Rememberinator --> Configurator 
  --> WindowDetails
  --> ScreenDetails
