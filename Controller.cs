﻿using System;
using System.Collections.Generic;

namespace WindozeScreenController
{
    public class Controller
    {
        public List<WindowDetail> CurrentWindows;
        public WindozeScreenController myWSC;
        public Rememberinator myRemberinator;

        public Controller(WindozeScreenController wsc)
        {
            myWSC = wsc;
            myRemberinator = new Rememberinator(wsc);

        }

        public void SaveCurrentConfiguration(string configName)
        {
            myRemberinator.AddConfiguration(configName);
            myRemberinator.Save();
        }

        public void FetchConfiguration(string configName)
        {
            myRemberinator.RestoreConfiguration(configName);
        }

        public void DeleteConfiguration(string configName)
        {
            myRemberinator.RemoveConfiguration(configName);
        }

    }
}
