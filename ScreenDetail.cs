﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace WindozeScreenController
{
	[Serializable]
	public class ScreenDetail
	{
		public int BitsPerPixel;
		public Rectangle Bounds;
		public string DeviceName;
		public bool Primary;
		public Rectangle WorkingArea;

		public ScreenDetail(Screen screen)
		{
			BitsPerPixel = screen.BitsPerPixel;
			Bounds = screen.Bounds;
			DeviceName = screen.DeviceName;
			Primary = screen.Primary;
			WorkingArea = screen.WorkingArea;
		}

		public bool IsEqual(Screen screenToCompare)
		{
			if (screenToCompare.DeviceName != DeviceName) { return false; }
			if (screenToCompare.BitsPerPixel != BitsPerPixel) { return false; }
			if (screenToCompare.Bounds != Bounds) { return false; }
			if (screenToCompare.Primary != Primary) { return false; }
			if (screenToCompare.WorkingArea != WorkingArea) { return false; }
			return true;
		}

	} // End public class ScreenSetup
}
